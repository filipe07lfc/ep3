class FoodRecipe < ApplicationRecord
	has_rich_text :ingredient
	has_rich_text :description
	has_rich_text :step

	mount_uploader :image, ImageUploader
	
	def self.search(search)
  # Title is for the above case, the OP incorrectly had 'name'
  where("title LIKE ?", "%#{search}%")
end
end
