class CreateReceita < ActiveRecord::Migration[6.0]
  def change
    create_table :receita do |t|
      t.string :title
      t.string :description

      t.timestamps
    end
  end
end
