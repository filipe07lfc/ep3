class CreateIngredients < ActiveRecord::Migration[6.0]
  def change
    create_table :ingredients do |t|
      t.text :content
      t.belongs_to :food_recipes, null: false, foreign_key: true

      t.timestamps
    end
  end
end
