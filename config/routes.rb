Rails.application.routes.draw do
	root 'food_recipes#index'
	get'search', to: "food_recipes#search"
	devise_for :users
	resources :food_recipes
end
