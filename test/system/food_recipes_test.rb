require "application_system_test_case"

class FoodRecipesTest < ApplicationSystemTestCase
  setup do
    @food_recipe = food_recipes(:one)
  end

  test "visiting the index" do
    visit food_recipes_url
    assert_selector "h1", text: "Food Recipes"
  end

  test "creating a Food recipe" do
    visit food_recipes_url
    click_on "New Food Recipe"

    fill_in "Description", with: @food_recipe.description
    fill_in "Title", with: @food_recipe.title
    click_on "Create Food recipe"

    assert_text "Food recipe was successfully created"
    click_on "Back"
  end

  test "updating a Food recipe" do
    visit food_recipes_url
    click_on "Edit", match: :first

    fill_in "Description", with: @food_recipe.description
    fill_in "Title", with: @food_recipe.title
    click_on "Update Food recipe"

    assert_text "Food recipe was successfully updated"
    click_on "Back"
  end

  test "destroying a Food recipe" do
    visit food_recipes_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Food recipe was successfully destroyed"
  end
end
